import React, { Component } from 'react';
import withCounter from './withCounter'
class ClickCounter extends Component {

  render() {
    const {count, incrementCount} = this.props
    return (
      <div>
        <button 
          className=""
          onClick={incrementCount}
        >
          Clicked {count} veces
        </button>
      </div>
    );
  }
}

export default withCounter(ClickCounter);