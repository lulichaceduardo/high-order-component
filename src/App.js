import React from 'react';
import './App.css';

import ClickCounter from './components/ClickCounter'
import HoverCounter from './components/HoverCounter'

class App extends React.Component {
  
  render() {
    return (
      <div className="App">
        <ClickCounter apellido="luli"/>
        <HoverCounter/>
      </div>
    );
  }
}

export default App;
