import React, { Component } from 'react';

const withCounter = (OriginalComponent) => {
  class NewComponent extends Component {
    constructor(props){
      super(props);
      this.state = {
        count: 0
      }
    }
    incrementCount = ()=>{
      const {count} = this.state;
      this.setState({count: count+1});
    }
    render() {
      return (<OriginalComponent 
          count={this.state.count}
          incrementCount = {this.incrementCount}
         {...this.props}/>)
    }
  }
  return NewComponent
}

export default withCounter;